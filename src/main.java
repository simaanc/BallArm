import processing.core.PApplet;

import java.io.File;

public class main extends PApplet {

    float bx;
    float by;
    int boxSize = 25;
    boolean overBox = false;
    boolean locked = false;
    float xOffset = (float) 0.0;
    float yOffset = (float) 0.0;

    int numSegments = 200;
    float[] x = new float[numSegments];
    float[] y = new float[numSegments];
    float[] angle = new float[numSegments];
    float segLength = 20;
    float targetX, targetY;

    public static void main(String[] args) {
        PApplet.main("main");
    }

    public void settings() {
        size(displayWidth, displayHeight);
        //smooth(8);
    }

    public void setup() {
        surface.setResizable(true);
        bx = (float) (width / 2.0);
        by = (float) (height / 2.0);

        x[x.length - 1] = displayWidth / 2;     // Set base x-coordinate
        y[x.length - 1] = displayHeight;  // Set base y-coordinate
    }

    public void draw() {
        background(204);

        // Tests if mouse is over the box
        overBox();

        drawReachSegment();

        // Draw the box
        strokeWeight(2);
        ellipse(bx, by, boxSize, boxSize);
    }

    public void mousePressed() {
        if (overBox) {
            locked = true;
            fill(255, 255, 255);
        } else {
            locked = false;
        }
        xOffset = mouseX - bx;
        yOffset = mouseY - by;

    }

    public void mouseDragged() {
        if (locked) {
            bx = mouseX - xOffset;
            by = mouseY - yOffset;
        }
    }

    public void mouseReleased() {
        locked = false;
    }

    void overBox() {
        // Test if the cursor is over the box
        if (mouseX > bx - boxSize && mouseX < bx + boxSize &&
                mouseY > by - boxSize && mouseY < by + boxSize) {
            overBox = true;
            if (!locked) {
                strokeWeight(2);
                stroke(255);
                fill(153);
            }
        } else {
            strokeWeight(2);
            stroke(153);
            fill(153);
            overBox = false;
        }
    }

    void drawReachSegment() {
        reachSegment(0, bx, by);
        for (int i = 1; i < numSegments; i++) {
            reachSegment(i, targetX, targetY);
        }
        for (int i = x.length - 1; i >= 1; i--) {
            positionSegment(i, i - 1);
        }
        for (int i = 0; i < x.length; i++) {
            segment(x[i], y[i], angle[i], ((i + 1) * 2));
        }
    }

    void positionSegment(int a, int b) {
        x[b] = x[a] + cos(angle[a]) * segLength;
        y[b] = y[a] + sin(angle[a]) * segLength;
    }

    void reachSegment(int i, float xin, float yin) {
        float dx = xin - x[i];
        float dy = yin - y[i];
        angle[i] = atan2(dy, dx);
        targetX = xin - cos(angle[i]) * segLength;
        targetY = yin - sin(angle[i]) * segLength;
    }

    void segment(float x, float y, float a, float sw) {
        strokeWeight(sw);
        pushMatrix();
        translate(x, y);
        rotate(a);
        line(0, 0, segLength, 0);
        popMatrix();
    }

    public void keyPressed() {
        if (key == 's') {
            selectFolder("Select a folder to precess:", "folderSelected");
        }
    }

    public void folderSelected(File selection) {
        if (selection == null) {
            return;
        } else {
            String dir = selection.getPath() + "\\";
            save(dir + "Structure.jpeg");
        }
    }
}
